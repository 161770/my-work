﻿using System;

namespace testing_variables
{
    class Program
    {

        private static int age;
        private static float number = 40.5f;
        private static string word = "help";


        static void Main(string[] args)
        {

            // This says hello world.
            Console.WriteLine("Hello World!");



            Double dubnum = 5.99D;

            age = 40;

            Console.WriteLine(age + " " + number + " " + word);

            float[] numbers = { age, number, age + number, age * number, age / number, 96, 72 };
            //for every single thing in the array, it will print an item
            foreach (var item in numbers)
            {
                Console.WriteLine(item);
            }

            bool thebool = true;
            thebool = false;
            char aletter = 'A';

        }
    }
}
